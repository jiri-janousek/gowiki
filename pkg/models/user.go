package models

// User model.
type User struct {
	ID       int
	Username string
	Email    string
	Password string
}
