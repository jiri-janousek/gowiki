package config

import (
	"fmt"
	"os"

	"gitlab.com/fenryxo/gowiki/pkg/logging"
)

const usageEnv = `Environment variables
GOWIKI_BIND: Hostname and port to bind.
GOWIKI_DATABASE_URL: Database connection URL.
GOWIKI_DATA_DIR: Directory to store data.
GOWIKI_LOG_LEVEL: The lowest log level to report.
`

// CollectEnv updates configuration from environment variables.
// May return an error derived from ErrInvalidValue on failure.
func CollectEnv(c *Config, getenv func(name string) (string, bool)) error {
	if v, ok := getenv("GOWIKI_BIND"); ok {
		c.Bind = v
	}

	if v, ok := getenv("GOWIKI_DATABASE_URL"); ok {
		c.DatabaseURL = v
	}

	if v, ok := getenv("GOWIKI_DATA_DIR"); ok {
		c.DataDir = v
	}

	if v, ok := getenv("GOWIKI_LOG_LEVEL"); ok {
		l, err := logging.LevelByName(v)
		if err != nil {
			return fmt.Errorf("%s: %w: %s", "GOWIKI_LOG_LEVEL", ErrInvalidValue, err)
		}

		c.LogLevel = l
	}

	return nil
}

// UsageEnv prints information about environment variables.
func UsageEnv() {
	fmt.Fprint(os.Stderr, usageEnv)
}
