// Package views represents the view layer of MVC.
package views

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"path/filepath"
)

const (
	// DefaultViewDir is the default subdirectory for view templates.
	DefaultViewDir = "views"
	// DefaultLayoutDir is the default subdirectory for layout templates.
	DefaultLayoutDir = "layouts"
)

// ErrTemplate is used for template errors.
var ErrTemplate = errors.New("template error")

// Loader loads views from files. You may create it with `NewLoader`.
type Loader struct {
	Ext       string
	ViewDir   string
	LayoutDir string
}

// NewLoader creates a new `Loader`.
func NewLoader(dir string, ext string) *Loader {
	return &Loader{
		Ext:       ext,
		ViewDir:   filepath.Join(dir, DefaultViewDir),
		LayoutDir: filepath.Join(dir, DefaultLayoutDir),
	}
}

// Load a view and return wrapped `ErrTemplate` on failure.
// `DefaultCode` is set to `http.StatusOK` and you may override it as needed.
func (l *Loader) Load(name string) (*View, error) {
	layouts, err := filepath.Glob(filepath.Join(l.LayoutDir, "*"+l.Ext))
	if err != nil {
		return nil, fmt.Errorf("layouts %w: %v", ErrTemplate, err)
	}

	t, err := template.ParseFiles(filepath.Join(l.ViewDir, name+l.Ext))
	if err != nil {
		return nil, fmt.Errorf("view %q %w: %v", name, ErrTemplate, err)
	}

	t, err = t.ParseFiles(layouts...)
	if err != nil {
		return nil, fmt.Errorf("layouts %w: %v", ErrTemplate, err)
	}

	return &View{Template: t, DefaultCode: http.StatusOK}, nil
}

// MustLoad loads a view or panics on failure.
// `DefaultCode` is set to `http.StatusOK` and you may override it as needed.
func (l *Loader) MustLoad(name string) *View {
	if v, err := l.Load(name); err != nil {
		panic(err)
	} else {
		return v
	}
}

// View handles rendering of output to user. You may create it with `Loader.Load` or `Loader.MustLoad`.
type View struct {
	// Template to render.
	Template *template.Template
	// DefaultCode to use with `RenderResponse`.
	DefaultCode int
}

// Render a HTML template into writer or return `html/template` error on failure.
func (v *View) Render(w io.Writer, ctx interface{}) error {
	err := v.Template.Execute(w, ctx)
	if err != nil {
		return fmt.Errorf("failed to render template %s: %w", v.Template.Name(), err)
	}

	return nil
}

// RenderResponse renders a HTML template as a HTTP response with HTTP status code specified
// by `DefaultCode` or returns `html/template` error on failure.
func (v *View) RenderResponse(w http.ResponseWriter, ctx interface{}) error {
	return v.RenderResponseCode(w, v.DefaultCode, ctx)
}

// RenderResponseCode renders a HTML template as a HTTP response with the provided HTTP status code
// or returns `html/template` error on failure.
func (v *View) RenderResponseCode(w http.ResponseWriter, code int, ctx interface{}) error {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(code)

	return v.Render(w, ctx)
}

// ServeHTTP renders response a HTML template as a HTTP response with HTTP status code specified
// by `DefaultCode` and `httpRequest` as template data. It panics on failure.
func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := v.RenderResponse(w, r); err != nil {
		panic(err)
	}
}
