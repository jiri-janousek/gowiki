// Package auth provides authorization and authentication functions.
package auth

import "golang.org/x/crypto/bcrypt"

// HashPassword returns a secure hash of a password.
func HashPassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 0)
	if err != nil {
		panic(err)
	}

	return string(hash)
}
