// Package stores is responsible for loading and saving models.
package stores

import "errors"

var (
	// ErrIO is used on input/output errors.
	ErrIO = errors.New("i/o error")
	// ErrNotExist is used when an object is not found.
	ErrNotExist = errors.New("not found")
)
