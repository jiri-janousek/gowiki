package main

import (
	"database/sql"
	"errors"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	// PostgreSQL driver.
	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/fenryxo/gowiki/pkg/config"
	"gitlab.com/fenryxo/gowiki/pkg/handlers"
	"gitlab.com/fenryxo/gowiki/pkg/logging"
	"gitlab.com/fenryxo/gowiki/pkg/mkd"
	"gitlab.com/fenryxo/gowiki/pkg/stores"
	"gitlab.com/fenryxo/gowiki/pkg/views"
)

func main() {
	c, err := config.Collect(os.Args[0], os.Args[1:], os.LookupEnv)
	if err != nil {
		if errors.Is(err, config.ErrArgHelp) {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}

	run(c)
}

func run(c *config.Config) {
	out := logging.NewLockedWriter(os.Stderr)
	log := logging.NewSimpleLogger(out, c.LogLevel)

	db, err := sql.Open("pgx", c.DatabaseURL)
	if err != nil {
		log.Panic("failed to open database", "error", err)
	}

	defer func() { _ = db.Close() }()

	err = db.Ping()
	if err != nil {
		log.Panic("failed to ping database", "error", err)
	}

	usersStore := stores.NewUsersDB(db)

	dir := "templates"
	l := views.NewLoader(dir, ".html")
	r := mux.NewRouter()

	notFoundHandler := l.MustLoad("404")
	notFoundHandler.DefaultCode = http.StatusNotFound
	r.NotFoundHandler = handlers.ErrorMiddleware(notFoundHandler.ServeHTTP)
	r.HandleFunc("/", handlers.ErrorMiddleware(handlers.Home))

	pageHandler := handlers.NewPage(
		l.MustLoad("create"),
		l.MustLoad("edit"),
		l.MustLoad("view"),
		stores.NewFilePage(c.DataDir, ".md"),
		mkd.NewRender(),
	)
	r.HandleFunc("/view/{page}", handlers.ErrorMiddleware(pageHandler.View))
	r.HandleFunc("/edit/{page}", handlers.ErrorMiddleware(pageHandler.Edit))

	usersHandler := handlers.NewUsers(l, usersStore)
	r.HandleFunc("/signup", handlers.ErrorMiddleware(usersHandler.SignUp))

	fileServer := http.FileServer(http.Dir("./static/"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fileServer))

	log.Info("listening", "address", c.Bind)

	if err := http.ListenAndServe(c.Bind, r); err != nil {
		log.Panic("failed to listen", "address", c.Bind, "error", err)
	}
}
