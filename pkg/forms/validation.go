package forms

import (
	"errors"
	"fmt"
)

// `ValidationError` codes.
const (
	// ValidationFailed because of unexpected reasons.
	ValidationFailed = "validation-failed"
	// TooManyValues were provided.
	TooManyValues = "too-many-values"
	// FieldRequired but not provided.
	FieldRequired = "field-required"
	// TooShort value was provided.
	TooShort = "too-short"
	// TooLong value was provided.
	TooLong = "too-long"
	// InvalidEmail value was provided.
	InvalidEmail = "invalid-email"
)

// ErrValidationFailed is a generic `ValidationError` for unexpected validation failures.
var ErrValidationFailed = &ValidationError{ValidationFailed, "Validation has failed."}

// `StringField` types.
const (
	// TextInput for entering ordinary text.
	TextInput = "text"
	// EmailInput for entering e-mail addresses.
	EmailInput = "email"
	// PasswordInput for entering passwords. Text is masked.
	PasswordInput = "password"
)

// ValidationError describes invalid fields.
type ValidationError struct {
	Code string
	Msq  string
}

func (e *ValidationError) Error() string {
	return e.Msq
}

// ErrList contains validation errors.
type ErrList []*ValidationError

// Messages returns validation error messages.
func (l *ErrList) Messages() []string {
	m := make([]string, len(*l))
	for i, err := range *l {
		m[i] = err.Msq
	}

	return m
}

// AddErr adds an error to the error list. The the error is not a `*ValidationError`,
// it is replaced with a generic `ErrValidationFailed`.
func (l *ErrList) AddErr(err error) {
	var valErr *ValidationError
	if !errors.As(err, &valErr) {
		valErr = ErrValidationFailed
	}

	*l = append(*l, valErr)
}

// Add a new `ValidationError`.
func (l *ErrList) Add(code, msg string) {
	*l = append(*l, &ValidationError{code, msg})
}

// Addf adds a new `ValidationError` with formatted message.
func (l *ErrList) Addf(code, msg string, data ...interface{}) {
	l.Add(code, fmt.Sprintf(msg, data...))
}
