package handlers

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/fenryxo/gowiki/pkg/mkd"
	"gitlab.com/fenryxo/gowiki/pkg/models"
	"gitlab.com/fenryxo/gowiki/pkg/stores"
	"gitlab.com/fenryxo/gowiki/pkg/views"
)

// Page handler data.
type Page struct {
	createView *views.View
	editView   *views.View
	viewView   *views.View
	pageStore  stores.Page
	mkdRender  mkd.Render
}

// NewPage creates a new page handler.
func NewPage(
	createView *views.View,
	editView *views.View,
	viewView *views.View,
	pageStore stores.Page,
	mkdRender mkd.Render,
) *Page {
	return &Page{
		createView: createView,
		editView:   editView,
		viewView:   viewView,
		pageStore:  pageStore,
		mkdRender:  mkdRender,
	}
}

// View page.
func (h *Page) View(w http.ResponseWriter, r *http.Request) {
	title := h.getTitle(r)

	p, err := h.pageStore.Load(title)
	if err != nil {
		must(h.createView.RenderResponseCode(w, http.StatusNotFound, title))
		return
	}

	mr := mkd.NewRender()
	body := template.HTML(mr(p.Body)) // #nosec G203

	ctx := map[string]interface{}{
		"Title": p.Title,
		"Body":  body,
	}

	must(h.viewView.RenderResponse(w, ctx))
}

// Edit page.
func (h *Page) Edit(w http.ResponseWriter, r *http.Request) {
	title := h.getTitle(r)

	var (
		p   *models.Page
		err error
	)

	if r.Method == "POST" {
		p = &models.Page{Title: title, Body: r.FormValue("body")}

		if r.FormValue("action") == "save" {
			if err := h.pageStore.Save(p); err != nil {
				panic(fmt.Errorf("%w: could not save %q: %v", ErrInternal, p.Title, err))
			}

			http.Redirect(w, r, "/view/"+title, http.StatusFound)

			return
		}
	} else {
		p, err = h.pageStore.Load(title)
		if err != nil {
			if !errors.Is(err, stores.ErrNotExist) {
				panic(fmt.Errorf("%w: could not load %q: %v", ErrInternal, title, err))
			}

			p = &models.Page{Title: title, Body: ""}
		}
	}

	mr := mkd.NewRender()
	body := template.HTML(mr(p.Body)) // #nosec G203

	ctx := map[string]interface{}{
		"Title":  p.Title,
		"Body":   body,
		"Source": p.Body,
	}

	must(h.editView.RenderResponse(w, ctx))
}

func (h *Page) getTitle(r *http.Request) string {
	vars := mux.Vars(r)
	title := vars["page"]

	if !models.IsTitleValid(title) {
		panic(ErrNotFound)
	}

	return title
}
