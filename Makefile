UID := $(shell id -u)
GID := $(shell id -g)
DB ?= postgresql://gowiki:gowiki@127.0.0.1:9100/gowiki

info:
	cat Makefile

run-images: stop-images
	mkdir -p build ~/.local/volumes/gowiki-db
	sed \
		-e 's/runAsUser:/\0 ${UID}/g' \
		-e 's/runAsGroup:/\0 ${GID}/g' \
		-e 's/fsGroup:/\0 ${GID}/g' \
		-e 's#~#${HOME}#g' \
    	localhost/pod.yml | tee build/pod.yml
	podman play kube build/pod.yml

stop-images:
	podman pod ps --format='{{.ID}}' -f 'label=app=gowiki' | xargs -r podman pod rm -f

psql:
	psql ${DB}

initdb:
	cd database && psql ${DB} -f schema.sql

build-app:
	cd cmd/gowiki && go build -o ../../build/gowiki -v

run-app:
	./reload.sh

help-app: build
	build/gowiki -h

lint:
	golangci-lint run

clean:
	rm -rf build
