package forms

import (
	"net/url"

	"gitlab.com/fenryxo/gowiki/pkg/models"
)

// `SignUp` validation errors.
var (
	// ErrDuplicateUsername is used to inform about duplicate username value.
	ErrDuplicateUsername = &ValidationError{"duplicate-username", "This username has already been taken."}
	// ErrInvalidUsername is used to inform about invalid username value.
	ErrInvalidUsername = &ValidationError{"invalid-username", "Username may contain only letters and digits."}
	// ErrDuplicateEmail is used to inform about duplicate e-mail value.
	ErrDuplicateEmail = &ValidationError{"duplicate-email", "This e-mail has already been registered."}
)

// SignUp form for users. Create with `NewSignUp` function and validate with `Validate` method.
type SignUp struct {
	FormErrors ErrList
	Valid      bool
	Username   StringField
	Email      StringField
	Password   StringField
}

// NewSignUp creates a new SignUp form. Validate it with `Validate` method.
func NewSignUp(usernameExists, emailExists func(v string) (bool, error)) *SignUp {
	return &SignUp{
		Username: StringField{
			Type:        TextInput,
			Name:        "username",
			Label:       "Username",
			Placeholder: "Enter username",
			Required:    true,
			MaxLength:   255, //nolint: gomnd
			Validators: []StringValidator{
				func(v string) error {
					if !models.IsTitleValid(v) {
						return ErrInvalidUsername
					}
					return nil
				},
				func(v string) error {
					if exists, err := usernameExists(v); err != nil {
						return err
					} else if exists {
						return ErrDuplicateUsername
					}
					return nil
				},
			},
		},
		Email: StringField{
			Type:        EmailInput,
			Name:        "email",
			Label:       "E-mail",
			Placeholder: "Enter e-mail",
			Required:    true,
			MaxLength:   255, //nolint: gomnd
			Validators: []StringValidator{
				func(v string) error {
					if exists, err := emailExists(v); err != nil {
						return err
					} else if exists {
						return ErrDuplicateEmail
					}
					return nil
				},
			},
		},
		Password: StringField{
			Type:        PasswordInput,
			Name:        "password",
			Label:       "Password",
			Placeholder: "Enter password",
			Required:    true,
			MaxLength:   255, //nolint: gomnd
		},
	}
}

// Validate provided `SignUp` form values, create validation errors and return the result.
func (f *SignUp) Validate(values url.Values) bool {
	f.Username.Validate(values[f.Username.Name])
	f.Email.Validate(values[f.Email.Name])
	f.Password.Validate(values[f.Password.Name])
	f.Valid = !f.HasErrors()

	return f.Valid
}

// HasErrors returns true if `SignUp` form has any validation errors.
// Both `FormErrors` and errors of individual fields are checked.
func (f *SignUp) HasErrors() bool {
	return len(f.FormErrors) > 0 ||
		len(f.Username.Errors) > 0 ||
		len(f.Email.Errors) > 0 ||
		len(f.Password.Errors) > 0
}
